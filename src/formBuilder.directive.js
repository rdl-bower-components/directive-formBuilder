angular.module('rdl.formBuilder', ['ui.select', 'ngSanitize', 'rdl.placeAutoCompleteForm', 'ngFileUpload']).directive('rdlFormBuilder', FormBuilderDirective);

export function FormBuilderDirective() {
  'ngInject';

  let directive = {
    restrict: 'E',
    templateUrl: 'formBuilder.html',
    scope: {
      fields: "=",
      savedCallback: "&?",
      item: "=?",
      onSave: "=?",
      showSaveBtn: "=",
      name: "@",
      saveText: "@?"

    },
    controller: FormBuilderController,
    controllerAs: 'vm',
    bindToController: true
  };

  return directive;
}

class FormBuilderController {
  constructor($scope, $injector, Upload) {
    'ngInject';

    this.Upload = Upload;
    this.saveText = (this.saveText !== undefined) ? this.saveText : 'action.save';
    this.onSave = () => this.saveItem();
    this.models = [];
    this.loading = [];

    this.keycloakService = null;
    try {
      this.keycloakService = $injector.get('keycloakService');
    } catch (err) {}

    $scope.$watch('vm.item', (newValue, oldValue, scope) => {
      if (angular.isDefined(newValue) && newValue !== null) {
        scope.vm.updateModels(scope.vm, newValue);
      }
    }, true);
  }


  saveItem(item) {
    this.updateItem();
    this.savedCallback({item: this.item});
  }

  updateModels(scope, item) {
    scope.item = item;
    for (var i = 0 ; i < scope.fields.length ; ++i) {
      if (scope.fields[i].type !== 'hr') {
        scope.models[i] = scope.getValue(item, scope.fields[i].key);
      }
      if (scope.fields[i].type === 'image') {
        scope.loading[i] = false;
      }
    }
  }

  updateItem() {
    for (var i = 0 ; i < this.fields.length ; ++i) {
      if (this.fields[i].type !== 'hr') {
        this.setValue(this.item, this.fields[i].key, this.models[i]);
      }
    }
  }

  getValue(item, name) {
    var items = name.split('.');
    var value = item[items[0]];
    for (var i = 1 ; i < items.length ; ++i) {
      if (!angular.isDefined(value) || value === null || angular.isUndefined(value[items[i]])) {
        return '';
      }
      value = value[items[i]];
    }
    return value;
  }

  setValue(row, name, value) {
    var items = name.split('.');
    var item = row;
    for (var i = 0 ; i < items.length - 1 ; ++i) {
      if (item[items[i]] === undefined) {
        item[items[i]] = {};
      }
      item = item[items[i]];
    }
    item[items[items.length - 1]] = value;
  }

  upload(url, file, index) {
    this.loading[index] = true;

    var config = {
      url: url,
      data: {file:  file}
    };
    if (this.keycloakService !== null) {
      config.headers = this.keycloakService.getHeaders();
    }
    this.Upload.upload(config).then(response  => {
      this.loading[index] = false;
      this.models[index] = response.data[0].location;
    }, error => {
      this.loading[index] = false;
    });
  }
}
