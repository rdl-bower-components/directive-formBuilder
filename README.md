Form builder
==========

# Description
Construct a form in function the given json

# Install

    bower install git@gitlab.com:rrddll-bower-components/directive-formBuilder.git --save

Then, add `rdl.formBuilder` in your index.module.js

# Exemple
```html
<rdl-form-builder
    item="ctrl.item"
    fields="ctrl.fields"
    show-save-btn="false"
    on-save="save"
    saved-callback="ctrl.saveItem(item)"
    name="myForm"
    save-text="action.save">
</rdl-form-builder>
```

# Directive attributes

## config (`required`)
    
The configuration of the different fields. The supported types are listed below :

### number
```javascript
{
  type: 'number',
  name: '_source.myValues',    //name of the field to display
  key: 'myAttr',               //attribute of the item to use - Can be in a sub-object like myObject1.myObject2.myValues
  required: true               //If not present and `true`, the field is mandatory
}
```

### text
```javascript
{
  type: 'text',
  name: '_source.myValues',    //name of the field to display, will be translated
  key: 'myAttr',               //attribute of the item to use - Can be in a sub-object like myObject1.myObject2.myValues
  required: true               //If not present and `true`, the field is mandatory
}
```

### textarea
```javascript
{
  type: 'textarea',
  name: '_source.myValues',    //name of the field to display, will be translated
  key: 'myAttr',               //attribute of the item to use - Can be in a sub-object like myObject1.myObject2.myValues
  rows: 10,                    //number of rows of the textarea
  required: true               //If not present and `true`, the field is mandatory
}
```

### multiselect
```javascript
{
  type: 'multiselect',
  name: '_source.myValues',    //name of the field to display, will be translated
  key: 'myAttr',               //attribute of the item to use - Can be in a sub-object like myObject1.myObject2.myValues
  values: ['a', 'b'],          //Array of possible values
  translateKey: 'myPref.',     //Translation prefix of the displayed item
  tagging: true,               //If not present and `true`, the multiselect accepts new tag on the fly
  required: true               //If not present and `true`, the field is mandatory
}
```

### select
```javascript
{
  type: 'select',
  name: '_source.myValues',    //name of the field to display, will be translated
  key: 'myAttr',               //attribute of the item to use - Can be in a sub-object like myObject1.myObject2.myValues
  values: ['a', 'b'],          //Array of possible values
  translateKey: 'myPref.',     //Translation prefix of the displayed item
  required: true               //If not present and `true`, the field is mandatory
}
```

### selectObject
```javascript
{
  type: 'select',
  name: '_source.myValues',    //name of the field to display, will be translated
  key: 'myAttr',               //attribute of the item to use - Can be in a sub-object like myObject1.myObject2.myValues
  values: ['a', 'b'],          //Array of possible values
  itemLabel: 'name',              //Attribute to use in the object for the key
  itemKey: 'id',              //Attribute to use in the object for the value
  required: true               //If not present and `true`, the field is mandatory
}
```

### place
```javascript
{
  type: 'place',
  name: '_source.myValues',    //name of the field to display if restrictType is true, will be translated
  key: 'myAttr',               //attribute of the item to use - Can be in a sub-object like myObject1.myObject2.myValues
  apiKey: 'qdqsij9.',          //Google Api key to use,
  restrictType: true           //See rdl-place-auto-complete for more information
  onlySearchInput: true        //See rdl-only-search-input for more information
}
```

### image
```javascript
{
  type: 'place',
  name: '_source.myValues',    //name of the field to display, will be translated
  key: 'myAttr',               //attribute of the item to use - Can be in a sub-object like myObject1.myObject2.myValues
  url: 'qdqsij9.',             //Backend url to use,
}
```

### hr
```javascript
{
  type: 'hr'
}
```

### label
```javascript
{
  type: 'label',
  name: '_source.myValues',    //name of the field to display, will be translated
  key: 'myAttr',               //attribute of the item to use - Can be in a sub-object like myObject1.myObject2.myValues
  translateKey: 'myPref.'      //Translation prefix of the displayed item
}
```

## saved-callback (`required`)

Callback called when the user click on the "save" button.
```javascript
onClickOnSave(item) {
  ...
}
```
## show-save-btn

If present and false, the save button is hidden, otherwise it is shown.

## on-save

Function that has to be called when the user save the form. If the save button is present,
it will do it automatically.

## item

The data used to fill the fields if necessary

## save-text

The text of the "save" button - will be translated.
By default the text is `action.save`.

## name

The name of form
