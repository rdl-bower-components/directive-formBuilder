/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(1);


/***/ },
/* 1 */
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	exports.FormBuilderDirective = FormBuilderDirective;

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	angular.module('rdl.formBuilder', ['ui.select', 'ngSanitize', 'rdl.placeAutoCompleteForm', 'ngFileUpload']).directive('rdlFormBuilder', FormBuilderDirective);

	function FormBuilderDirective() {
	  'ngInject';

	  var directive = {
	    restrict: 'E',
	    template: '<ng-form name="vm.{{vm.name}}" class="form-horizontal form-builder"><div class="form-group" ng-repeat="field in vm.fields"><div ng-if="field.type === \'text\'"><label for="{{field.key}}" class="col-xs-2 control-label">{{field.name | translate}}</label><div class="col-xs-10"><input type="text" class="form-control" id=\'{{field.key}}"\' ng-model="vm.models[$index]" ng-required="field.required"></div></div><div ng-if="field.type === \'number\'"><label for="{{field.key}}" class="col-xs-2 control-label">{{field.name | translate}}</label><div class="col-xs-10"><input type="number" class="form-control" id=\'{{field.key}}"\' ng-model="vm.models[$index]" ng-required="field.required"></div></div><div ng-if="field.type === \'price\'"><label for="{{field.key}}" class="col-xs-2 control-label">{{field.name | translate}}</label><div class="col-xs-8"><input type="number" class="form-control" id=\'{{field.key}}"\' ng-model="vm.models[$index].value" ng-change="vm.updateItem()" ng-required="field.required"></div><div class="col-xs-2"><ui-select class="multi-value-select" ng-model="vm.models[$index].currency" theme="bootstrap" ng-change="vm.updateItem()" id="{{field.key}}" required="field.required"><ui-select-match>{{$select.selected | translate}}</ui-select-match><ui-select-choices repeat="item in field.values | filter:$select.search">{{item | translate}}</ui-select-choices></ui-select></div></div><div ng-if="field.type === \'textarea\'"><label ng-if="field.name" class="control-label col-xs-2" for="{{field.key}}">{{field.name | translate}}</label><div ng-class="{\'col-xs-10\' : field.name, \'col-xs-12\' : !field.name}"><textarea class="form-control" ng-model="vm.models[$index]" rows="{{field.rows || 5}}" id="{{field.key}}" ng-required="field.required">\n            </textarea></div></div><div ng-if="field.type === \'multiselect\'"><label class="col-xs-2 control-label" for="{{field.key}}">{{field.name | translate}}</label><div class="col-xs-10"><ui-select class="multi-value-select" multiple ng-model="vm.models[$index]" theme="bootstrap" id="{{field.key}}" close-on-select="false" sortable="true" ng-attr-tagging="{{field.tagging || undefined}}" tagging-label="false" required="field.required"><ui-select-match>{{field.translateKey + $item | translate}}</ui-select-match><ui-select-choices repeat="item in field.values | filter:$select.search">{{field.translateKey + item | translate}}</ui-select-choices></ui-select></div></div><div ng-if="field.type === \'select\'"><label class="col-xs-2 control-label" for="{{field.key}}">{{field.name | translate}}</label><div class="col-xs-10"><ui-select class="single-value-select" ng-model="vm.models[$index]" id="{{field.key}}" theme="bootstrap" required="field.required"><ui-select-match>{{field.translateKey + $select.selected | translate}}</ui-select-match><ui-select-choices repeat="item in field.values | filter:$select.search">{{field.translateKey + item | translate}}</ui-select-choices></ui-select></div></div><div ng-if="field.type === \'selectObject\'"><label class="col-xs-2 control-label" for="{{field.key}}">{{field.name | translate}}</label><div class="col-xs-10"><ui-select class="single-value-select" ng-model="vm.models[$index]" id="{{field.key}}" theme="bootstrap" required="field.required"><ui-select-match>{{$select.selected.value[field.itemLabel]}}</ui-select-match><ui-select-choices repeat="label.value[field.itemKey] as (key, label) in field.values | filter:$select.search">{{label.value[field.itemLabel] | translate}}</ui-select-choices></ui-select></div></div><div ng-if="field.type === \'place\' && field.onlySearchInput"><label class="control-label col-xs-2">{{field.name | translate}}</label><div class="col-xs-10"><rdl-place-auto-complete-form api-key="{{field.apiKey}}" id="{{field.key}}" only-search-input ng-attr-restrict-type="{{field.restrictType || undefined}}" location="vm.models[$index]"></rdl-place-auto-complete-form></div></div><div ng-if="field.type === \'place\' && !field.onlySearchInput" class="col-xs-12"><div><rdl-place-auto-complete-form api-key="{{field.apiKey}}" id="{{field.key}}" ng-attr-restrict-type="{{field.restrictType || undefined}}" location="vm.models[$index]"></rdl-place-auto-complete-form></div></div><div ng-if="field.type === \'image\'"><label for="{{field.key}}" class="col-xs-2 control-label">{{field.name | translate}}</label><div class="col-xs-10 label-value input-group"><input type="text" class="form-control" id=\'{{field.key}}"\' ng-model="vm.models[$index]" ng-required="field.required"> <a class="btn-primary input-group-addon" accept="image/*" id="{{field.key}}" ngf-select="vm.upload(field.url, $file, $index)" ngf-drop><span ng-if="vm.loading[$index] === false"><i class="fa fa-upload" aria-hidden="true"></i></span> <span ng-if="vm.loading[$index] === true"><i class="fa fa-spinner fa-pulse fa-1x fa-fw"></i></span></a></div></div><div ng-if="field.type === \'label\'"><label for="{{field.key}}" class="col-xs-2 control-label">{{field.name | translate}}</label><div class="col-xs-10 label-value">{{field.translateKey + vm.models[$index] | translate}}</div></div><hr ng-if="field.type === \'hr\'"></div><div class="row" ng-if="vm.showSaveBtn"><div class="col-xs-10 col-sm-offset-2"><button class="btn btn-primary pull-left" type="button" ng-click="vm.saveItem()" ng-disabled="vm[vm.name].$invalid">{{vm.saveText | translate}}</button></div></div></ng-form>',
	    scope: {
	      fields: "=",
	      savedCallback: "&?",
	      item: "=?",
	      onSave: "=?",
	      showSaveBtn: "=",
	      name: "@",
	      saveText: "@?"

	    },
	    controller: FormBuilderController,
	    controllerAs: 'vm',
	    bindToController: true
	  };

	  return directive;
	}

	var FormBuilderController = function () {
	  FormBuilderController.$inject = ["$scope", "$injector", "Upload"];
	  function FormBuilderController($scope, $injector, Upload) {
	    'ngInject';

	    var _this = this;

	    _classCallCheck(this, FormBuilderController);

	    this.Upload = Upload;
	    this.saveText = this.saveText !== undefined ? this.saveText : 'action.save';
	    this.onSave = function () {
	      return _this.saveItem();
	    };
	    this.models = [];
	    this.loading = [];

	    this.keycloakService = null;
	    try {
	      this.keycloakService = $injector.get('keycloakService');
	    } catch (err) {}

	    $scope.$watch('vm.item', function (newValue, oldValue, scope) {
	      if (angular.isDefined(newValue) && newValue !== null) {
	        scope.vm.updateModels(scope.vm, newValue);
	      }
	    }, true);
	  }

	  _createClass(FormBuilderController, [{
	    key: 'saveItem',
	    value: function saveItem(item) {
	      this.updateItem();
	      this.savedCallback({ item: this.item });
	    }
	  }, {
	    key: 'updateModels',
	    value: function updateModels(scope, item) {
	      scope.item = item;
	      for (var i = 0; i < scope.fields.length; ++i) {
	        if (scope.fields[i].type !== 'hr') {
	          scope.models[i] = scope.getValue(item, scope.fields[i].key);
	        }
	        if (scope.fields[i].type === 'image') {
	          scope.loading[i] = false;
	        }
	      }
	    }
	  }, {
	    key: 'updateItem',
	    value: function updateItem() {
	      for (var i = 0; i < this.fields.length; ++i) {
	        if (this.fields[i].type !== 'hr') {
	          this.setValue(this.item, this.fields[i].key, this.models[i]);
	        }
	      }
	    }
	  }, {
	    key: 'getValue',
	    value: function getValue(item, name) {
	      var items = name.split('.');
	      var value = item[items[0]];
	      for (var i = 1; i < items.length; ++i) {
	        if (!angular.isDefined(value) || value === null || angular.isUndefined(value[items[i]])) {
	          return '';
	        }
	        value = value[items[i]];
	      }
	      return value;
	    }
	  }, {
	    key: 'setValue',
	    value: function setValue(row, name, value) {
	      var items = name.split('.');
	      var item = row;
	      for (var i = 0; i < items.length - 1; ++i) {
	        if (item[items[i]] === undefined) {
	          item[items[i]] = {};
	        }
	        item = item[items[i]];
	      }
	      item[items[items.length - 1]] = value;
	    }
	  }, {
	    key: 'upload',
	    value: function upload(url, file, index) {
	      var _this2 = this;

	      this.loading[index] = true;

	      var config = {
	        url: url,
	        data: { file: file }
	      };
	      if (this.keycloakService !== null) {
	        config.headers = this.keycloakService.getHeaders();
	      }
	      this.Upload.upload(config).then(function (response) {
	        _this2.loading[index] = false;
	        _this2.models[index] = response.data[0].location;
	      }, function (error) {
	        _this2.loading[index] = false;
	      });
	    }
	  }]);

	  return FormBuilderController;
	}();

/***/ }
/******/ ]);